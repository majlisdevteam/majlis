
import { GroupEventModel } from "./group-event";
import { NotificationModel } from "./majlis-notification";
import { MajlisMdCode } from "./majlis-md-code";
import { MajlisCMSUser } from "./cms-user";

export class MajlisGroupModel {

    groupId = null;
    groupSystemId = null;
    groupTitle = null;
    groupIconPath = null;
    groupDescription = null;
    groupCountry = null;
    groupDistrict = null;
    groupCity = null;
    userInserted = null;
    dateInserted = null;
    userModified = null;
    dateModified = null;
    majlisGroupEventCollection: Array<GroupEventModel> = null;
    majlisNotificationCollection:Array<NotificationModel> = null;
    groupPublicLevel:MajlisMdCode = null;
    groupAdmin:MajlisCMSUser = null;
    groupStatus: MajlisMdCode = null;
    
    


}
