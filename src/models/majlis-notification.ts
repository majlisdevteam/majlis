import { MajlisGroupModel } from "./majlis-group";
import { GroupEventModel } from "./group-event";
import { MajlisCMSUser } from "./cms-user";
import { MajlisMdCode } from "./majlis-md-code";

export class NotificationModel {
    notificationId = null;
    notificationSystemId = null;
    notificationIconPath = null;
    notificationMessage = null;
    dateInserted = null;
    notificationGroupId: MajlisGroupModel = null;
    notificationEventId: GroupEventModel = null;
    notificationUserId: MajlisCMSUser = null;
    notificationStatus: MajlisMdCode = null;

}