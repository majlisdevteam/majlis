import { MajlisMdCode } from "./majlis-md-code";

export class BirthdayTemplate {
    templateId = null;
    templateSystemId = null;
    templateTitle = null;
    templateIconPath = null;
    templateMessage = null;
    dateInserted = null;
    userInserted = null;
    dateModified = null;
    userModified = null;
    templateStatus : MajlisMdCode = null;
    
}