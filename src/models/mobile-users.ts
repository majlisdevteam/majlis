
import { MobileUsersEventCategoryModel } from "./mobileusers-event-category";

export class MobileUserModel {

mobileUserId = null;
mobileUserMobileNo = null;
mobileUserName = null;
mobileUserEmail = null;
mobileUserDob = null;
mobileUserCountry = null;
mobileUserProvince = null;
mobileUserCity = null;
mobileUserDateRegistration = null;
mobileUserDateModified = null;
mobileUserPassCode = null;
majlisMobileUsersCategoryCollection : Array<MobileUsersEventCategoryModel> = null;

}