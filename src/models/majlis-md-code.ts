import { GroupEventModel } from "./group-event";
import { NotificationModel } from "./majlis-notification";
import { EventCategoryModel } from "./event-category";
import { MajlisCMSUser } from "./cms-user";
import { BirthdayTemplate } from "./birthday-template";
import { MajlisGroupModel } from "./majlis-group";

export class MajlisMdCode {
    codeId = null;
    codeType = null;
    codeSubType = null;
    codeLocale = null;
    codeSeq = null;
    codeMessage = null;
    majlisGroupEventCollection: Array<GroupEventModel> = null;
    majlisNotificationCollection: Array<NotificationModel> = null;
    majlisCategoryCollection: Array<EventCategoryModel> = null;
    majlisCmsUsersCollection: Array<MajlisCMSUser> = null;
    majlisBirthdayTemplateCollection: Array<BirthdayTemplate> = null;
    majlisGroupCollection: Array<MajlisGroupModel> = null;
    majlisGroupCollection1:Array<MajlisGroupModel> = null;


}