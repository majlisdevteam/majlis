import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { EventWall } from "../event-wall/event-wall";
import { CountryList } from "../country-list/country-list";
import { Storage } from '@ionic/storage';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { MobileUserModel } from "../../models/mobile-users";
import { MobileUserService } from "../../providers/mobile-users-service";
import { RequestOptionsArgs, RequestOptions, Headers } from "@angular/http";
import { EventCategoryModel } from "../../models/event-category";
import { MobileUsersEventCategoryModel } from "../../models/mobileusers-event-category";



/**
 * Generated class for the Registration page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html',
  providers: [MobileUserModel, MobileUserService]
})
export class RegistrationPage {

  userRegistrationForm: FormGroup;


  countryName: string = "United Arab Emirates";
  countryFlag: string;
  countryCode: string = "AE";

  //forum data
  mobileNo: any = null;
  userName: string = null;
  emailAddress: string = null;
  dateOfBirth: any = null;
  country: string = null;
  province: string = null;
  city: string = null;
  category: Array<MobileUsersEventCategoryModel> = [];

  public loginForm = this.fb.group({
    mobileNo: ["", Validators.required],
    userName: ["", Validators.required]
    /*emailAddress: ["", Validators.email],
    dateOfBirth: ["", Validators.required],
    province: ["", Validators.required],
    city: ["", Validators.required],
    category: ["", Validators],*/
  });

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private modalCtrl: ModalController, public storage: Storage,
    public fb: FormBuilder, private sqlite: SQLite, public mobileUser: MobileUserModel,
    public mobileUserService: MobileUserService) {


  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad Registration');

    this.sqlite.create({
      name: "MajlisUser.db",
      location: "default"
    }).then((database: SQLiteObject) => {
      database.executeSql("CREATE TABLE IF NOT EXISTS tbl_MajlisUser (Id INTEGER PRIMARY KEY AUTOINCREMENT, MobileNo VARCHAR(15), Name TEXT, Email VARCHAR(200), DateOfBirth DATE, Country Varchar(200), Province Varchar(200), City Varchar(200), Category Varchar(500))", {}).then((data) => {
        console.log("TABLE MajlisUser CREATED: ", data);
        //this.refresh();
      }, (error) => {
        console.error("Unable to execute sql MajlisUser", error);
      });
    }, (error) => {
      console.error("Unable to open database", error);
    });
  }


  public databseConnection() {
    return this.sqlite.create({
      name: "AssetManager.db",
      location: "default"
    });
  }



  goToEventWall(IsSkip: boolean) {
    this.navCtrl.setRoot(EventWall);
  }

  openCountryList($event) {
    let modalCountry = this.modalCtrl.create(CountryList);
    modalCountry.onDidDismiss(data => {
      console.log("data ", data);

      if (data != undefined) {

        this.countryName = data.name;
        this.countryFlag = data.flag;
        this.countryCode = data.countryCode;
        //this.countryName = data.name;

      }
    });

    modalCountry.present()

  }

  registerUser() {

    this.storage.set("is-registered", "registered")
    this.storage.set('mobile-no', this.mobileNo);
    this.storage.set('user-name', this.userName);
    this.storage.set('email-address', this.emailAddress);
    this.storage.set('date-of-birth', this.dateOfBirth);
    this.storage.set('country', this.countryName);
    this.storage.set('province', this.province);
    this.storage.set('city', this.city);
    this.storage.set('category', this.category);
    console.log(this.mobileNo, this.userName, this.emailAddress, this.dateOfBirth, this.countryName, this.province, this.province, this.city, this.category);


    //binding with the service
    this.mobileUser.mobileUserMobileNo = this.mobileNo;
    this.mobileUser.mobileUserName = this.userName;
    this.mobileUser.mobileUserEmail = this.emailAddress;
    this.mobileUser.mobileUserDob = this.dateOfBirth;
    this.mobileUser.mobileUserCountry = this.country;
    this.mobileUser.mobileUserProvince = this.province;
    this.mobileUser.mobileUserCity = this.city;

    let mobileCategory: MobileUsersEventCategoryModel = new MobileUsersEventCategoryModel();

    let eventCatModel: EventCategoryModel = new EventCategoryModel();

    eventCatModel.categoryId = 1;

    mobileCategory.majlisCategory = eventCatModel;

    //let  category : EventCategoryModel = new EventCategoryModel();

    this.category.push(mobileCategory);

    console.log("category ", this.category);


    this.mobileUser.majlisMobileUsersCategoryCollection = this.category;

    //this.mobileUser.majlisMobileUsersCategoryCollection = this.category;


    console.log("mobileUser ", this.mobileUser);


    let header: Headers = new Headers();

    let option: RequestOptionsArgs = new RequestOptions();

    // header.set("system", "MajlisCMS");

    // option.headers = header;

    this.mobileUserService.saveMobileUsers(this.mobileUser).subscribe(res => {
      console.log("success ", res);
    },
      error => {
        console.log("success ", error);
      })



  }

}
