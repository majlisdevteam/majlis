import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AppParams } from "../../app/app.module";
import { AbstractControl } from "@angular/forms/src/forms";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


/**
 * Generated class for the AboutUs page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-about-us',
  templateUrl: 'about-us.html',
})
export class AboutUs {

  private about: any


  slideOneForm: FormGroup;


  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder) {
    this.slideOneForm = formBuilder.group({
      firstName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      lastName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      age: [''],
      category: ['']
    });

  }

  save() {
    console.log(this.slideOneForm.value);
    let obj = this.slideOneForm.value;
    console.log(obj.age);
    console.log(obj.category);
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutUs');
    this.about = AppParams.ABOUT;
  }

}
