import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { MediaCapture, CaptureVideoOptions, CaptureError, MediaFile } from "@ionic-native/media-capture";
import { Transfer, TransferObject } from "@ionic-native/transfer";
import { File } from '@ionic-native/file';


//import { MediaCapture } from '@ionic-native/media-capture';

/**
 * Generated class for the AddImagesVideos page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-add-images-videos',
  templateUrl: 'add-images-videos.html',
})
export class AddImagesVideos {

  @ViewChild('myvideo') myVideo: any;

  public photos: any;
  public base64Image: string;
  base64Image1
  constructor(public navCtrl: NavController, public navParams: NavParams, private camera: Camera, private alertCtrl: AlertController, private mediaCapture: MediaCapture, private transfer: Transfer, private file: File) {

  }

  ngOnInit() {
    this.photos = [];
  }

  takePhoto() {
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.base64Image = 'data:image/jpeg;base64,' + imageData;
      this.photos.push(this.base64Image);
      this.photos.reverse();
    }, (err) => {
      // Handle error
    });
  }

  deletePhoto(index) {

    let alert = this.alertCtrl.create({
      title: 'Delete selected photo?',
      message: '',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'OK',
          handler: () => {
            this.photos.splice(index, 1);
          }
        }
      ]
    });
    alert.present();
  }

  /*accessGallery(){
     this.camera.getPicture({
       sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
       destinationType: this.camera.DestinationType.DATA_URL
      }).then((imageData) => {
        this.base64Image1 = 'data:image/jpeg;base64,'+imageData;
       }, (err) => {
        console.log(err);
      });
    }
  
    ionViewDidLoad() {
      console.log('ionViewDidLoad AddImagesVideos');
    }
  */








  startRecording() {
    let options: CaptureVideoOptions = { limit: 1, duration: 10, quality: 20 };
    this.mediaCapture.captureVideo(options)
      .then(((data: MediaFile[]) => {

        data.forEach((key, value) => {
          console.log("key", key.fullPath);
          // console.log("value",value );
          const fileTransfer: TransferObject = this.transfer.create();

          let url = "http://192.0.0.46:8585";
          fileTransfer.upload(key.fullPath, url)
            .then((data) => {
              // success
              console.log("########### upload success ##########");
            }, (err) => {
              // error
              console.log("########## upload error ###########",err
              );
            })

//console.log(">>>>>>>>>>>>>>>>>>>>>");





        })


      }), ((error: CaptureError) => {

        console.log("###### Error #########")
      })

      // (data: MediaFile[]) => console.log(data),
      // (err: CaptureError) => console.error(err)
      )
    //console.log('Video Recorded. Result: ' + JSON.stringify(options));

  }



  selectVideo() {
    let video = this.myVideo.nativeElement;
    var options = {
      sourceType: 2,
      mediaType: 1,
      duration: 10
    };
    this.camera.getPicture(options).then((data) => {
      video.src = data;
      video.play();
    })
  }
}
