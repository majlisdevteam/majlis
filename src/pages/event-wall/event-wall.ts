import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Notifications } from "../notifications/notifications";
import { NearbyEvents } from "../nearby-events/nearby-events";
import { EventComments } from "../event-comments/event-comments";

/**
 * Generated class for the EventWall page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-event-wall',
  templateUrl: 'event-wall.html',
})
export class EventWall {
  items = [];
  eventTitle: String = "Live Music Night";

  constructor(public navCtrl: NavController, public navParams: NavParams,private alertCtrl: AlertController) {
    for (let i = 0; i < 20; i++) {
      this.items.push( this.items.length );
    }
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad EventWall');
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    setTimeout(() => {
      for (let i = 0; i < 20; i++) {
        this.items.push( this.items.length );
      }

      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 500);
  }

  openNotificationsPage(){
        this.navCtrl.push(Notifications);
  }

    openSelectedEventPage(){
        this.navCtrl.push(NearbyEvents);
  }

  openEventCommentsPage(){
    let data = {
      eventTitle: this.eventTitle
    }
        this.navCtrl.push(EventComments,data);
  }

  eventStatusAlert() {
  let alert = this.alertCtrl.create({
    title: 'Status',
    inputs: [
      {
        type: 'radio',
        label: 'Not Attending',
        value: 'notattend',
        checked: true
      },
      {
        type: 'radio',
        label: 'Attending',
        value: 'attend'
      },
      {
        type: 'radio',
        label: 'Maybe',
        value: 'maybe'
      }
    ],
    buttons : [
      {
        text: 'Cancel'
      },
      {
        text: 'Ok',
        handler: (data: any) => {
          console.log('Radio data:', data);
        }
      }
    ]
  });
  alert.present();
}

doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }
  

}
