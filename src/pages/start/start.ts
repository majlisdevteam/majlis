import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TermsPage } from "../terms/terms";
import { DisclaimerPage } from "../disclaimer/disclaimer";
import { RegistrationPage } from "../registration/registration";
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';



/**
 * Generated class for the Start page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-start',
  templateUrl: 'start.html',
})
export class Start {

  constructor(public navCtrl: NavController, public navParams: NavParams, private sqlite: SQLite) {

    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Start');

  }

  openTerms() {
    this.navCtrl.push(TermsPage);
  }

  opendisclaimer() {
    this.navCtrl.push(DisclaimerPage);
  }

  acceptTerms() {
    this.navCtrl.push(RegistrationPage);
  }

}
