import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, FabContainer } from 'ionic-angular';
import { EventComments } from "../event-comments/event-comments";
import { AddImagesVideos } from "../add-images-videos/add-images-videos";
import { Feedback } from "../feedback/feedback";
import { Calendar } from '@ionic-native/calendar';

/**
 * Generated class for the NearbyEvents page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
declare var google;

@IonicPage()
@Component({
  selector: 'page-nearby-events',
  templateUrl: 'nearby-events.html',
})
export class NearbyEvents {
  //public startDate:Date;
  public startDate = new Date("May 18, 2017 10:13:00");
  public endDate = new Date("May 19, 2017 10:13:00");
  public abc: any;


  fabButtonOpened: Boolean;
  eventTitle: String = "Live Music Night";
  eventImages = ['1.jpg', '2.jpg'];


  @ViewChild('map') mapElement;
  map: any;


  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, public calendar: Calendar, private loadingCtrl: LoadingController) {
    this.fabButtonOpened = false;
  }


  createEvent(fab: FabContainer) {
    fab.close();
    this.calendar.createEvent("Live Music Night ", "Adelaide", "Please join with Live Music Night", this.startDate, this.endDate);
    let alert = this.alertCtrl.create({
      title: 'New Event!',
      message: 'Event successfully added to calendar',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            console.log('OK clicked');
          }
        }
      ]
    });
    alert.present();

  }
  checkEvent() { /*
  //let newEvent = this.calendar.createEvent("Live Music Night ", "Adelaide", "Please join with Live Music Night", this.startDate, this.endDate);
  var x = this.calendar.findEvent("Live Music Night ", "Adelaide", "Please join with Live Music Night", this.startDate, this.endDate);
  x.then(result =>{
    let alert = this.alertCtrl.create({
    title: 'Event same',
    message: '',
    buttons: [
      {
        text: 'OK',
        role: 'cancel',
        handler: () => {
          console.log('OK clicked');
        }
      }
    ]
  });
  alert.present();
    console.log("Success!", result);
  },fail => {
    let alert = this.alertCtrl.create({
    title: 'Event not same',
    message: '',
    buttons: [
      {
        text: 'OK',
        role: 'cancel',
        handler: () => {
          console.log('OK clicked');
        }
      }
    ]
  });
  alert.present();
    console.log("Failed!", fail);
  });

*/

    /*if(){
      let alert = this.alertCtrl.create({
      title: 'Event same',
      message: '',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            console.log('OK clicked');
          }
        }
      ]
    });
    alert.present();
    }else{
        let alert = this.alertCtrl.create({
      title: 'Event not same',
      message: '',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            console.log('OK clicked');
          }
        }
      ]
    });
    alert.present();
    } */
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NearbyEvents');
    let loader = this.loadingCtrl.create({
      content: "Loading",
    });

    loader.present();
    this.initMap();
    loader.dismiss();
  }

  initMap() {
    let latLng = new google.maps.LatLng(-34.9290, 138.6010);

    let mapOptions = {
      center: latLng,
      zoom: 15,
      draggable: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.addMarker(-34.9290, 138.6010);
  }

  addMarker(long, lat) {

    let latLng = new google.maps.LatLng(long, lat);
    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: latLng,
      icon: "assets/img/mapmarker.png",
    });

    //let content = "<h4>Information!</h4>";  
    //let content = des;

    this.addInfoWindow(marker, this.eventTitle);

  }

  addInfoWindow(marker, content) {

    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });

  }


  eventStatusAlert() {
    let alert = this.alertCtrl.create({
      title: 'Status',
      inputs: [
        {
          type: 'radio',
          label: 'Not Attending',
          value: 'notattend',
          checked: true
        },
        {
          type: 'radio',
          label: 'Attending',
          value: 'attend'
        },
        {
          type: 'radio',
          label: 'Maybe',
          value: 'maybe'
        }
      ],
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text: 'Ok',
          handler: (data: any) => {
            console.log('Radio data:', data);
          }
        }
      ]
    });
    alert.present();
  }

  openEventCommentsPage(fab: FabContainer) {
    fab.close();
    let data = {
      eventTitle: this.eventTitle
    }
    this.navCtrl.push(EventComments, data);
  }
  openImageVideo(fab: FabContainer) {
    fab.close();
    this.navCtrl.push(AddImagesVideos);
  }
  openFeedback(fab: FabContainer) {
    fab.close();
    this.navCtrl.push(Feedback);
  }
  openFabButton() {
    if (this.fabButtonOpened == false) {
      this.fabButtonOpened = true;
    } else {
      this.fabButtonOpened = false;
    }
  }

}
