import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the EventComments page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-event-comments',
  templateUrl: 'event-comments.html',
})
export class EventComments {

  comments= [];
  eventTitle:String;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    
        for (let i = 0; i < 30; i++) {
          this.comments.push( this.comments.length );
        }
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    setTimeout(() => {
      for (let i = 0; i < 30; i++) {
        this.comments.push( this.comments.length );
      }

      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 500);
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventComments');
    this.eventTitle = this.navParams.get('eventTitle');
  }

}
