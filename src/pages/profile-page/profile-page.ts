import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { CountryList } from "../country-list/country-list";


/**
 * Generated class for the ProfilePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-profile-page',
  templateUrl: 'profile-page.html'
})
export class ProfilePage {

  //forum data
    mobileNo: any;
    userName: string;
    emailAddress: string;
    dateOfBirth: any;
    country: string;
    province: string;
    city: string;
    category: any;

    countryFlag: string;
    countryCode: string = "AE";

  constructor(public navCtrl: NavController, public navParams: NavParams,private modalCtrl: ModalController, public storage: Storage, private alertCtrl: AlertController) {
    this. getProfileData();
  }


  getProfileData(){
    this.storage.get('mobile-no').then(mobileNo=>{
      this.mobileNo = mobileNo;

    })
    this.storage.get('user-name').then(userName=>{
      this.userName = userName;
    })
    this.storage.get('email-address').then(emailAddress=>{
      this.emailAddress = emailAddress;
    })
    this.storage.get('date-of-birth').then(dateOfBirth=>{
      this.dateOfBirth = dateOfBirth;
    })
    this.storage.get('country').then(country=>{
      if(country == null){
        this.country = "United Arab Emirates";
      }else{
        this.country = country;
      }
    })
    this.storage.get('province').then(province=>{
      this.province = province;
    })
    this.storage.get('city').then(city=>{
      this.city = city;
    })
    this.storage.get('category').then(category=>{
      this.category = category;
    })
  }

  openCountryList($event) {
    let modalCountry = this.modalCtrl.create(CountryList);
    modalCountry.onDidDismiss(data => {
      console.log("data ", data);

      if (data != undefined) {

        this.country = data.name;
        this.countryFlag = data.flag;
        this.countryCode = data.countryCode;
        this.country = data.name;
        
      }
    });

    modalCountry.present()

  }

    updateUser(){
    this.storage.set('mobile-no', this.mobileNo);
    this.storage.set('user-name', this.userName);
    this.storage.set('email-address', this.emailAddress);
    this.storage.set('date-of-birth', this.dateOfBirth);
    this.storage.set('country', this.country);
    this.storage.set('province', this.province);
    this.storage.set('city', this.city);
    this.storage.set('category', this.category);
    console.log(this.mobileNo, this.userName,this.emailAddress,this.dateOfBirth,this.country,this.province,this.province,this.city,this.category);
    this.presentConfirm();
  }


presentConfirm() {
  let alert = this.alertCtrl.create({
    message: 'Your profile has been successfully updated!',
    buttons: [
      {
        text: 'OK',
        role: 'cancel',
        handler: () => {
          console.log('OK clicked');
        }
      }
    ]
  });
  alert.present();
}
  


  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

}
