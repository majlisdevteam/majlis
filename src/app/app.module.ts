import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { IonicStorageModule } from '@ionic/storage';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { Camera } from '@ionic-native/camera';
import { Calendar } from '@ionic-native/calendar';
import { MediaCapture } from "@ionic-native/media-capture";
import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Start } from "../pages/start/start";
import { TermsPage } from "../pages/terms/terms";
import { DisclaimerPage } from "../pages/disclaimer/disclaimer";
import { RegistrationPage } from "../pages/registration/registration";
import { EventWall } from "../pages/event-wall/event-wall";
import { CountryList } from "../pages/country-list/country-list";
import { HttpModule } from "@angular/http";
import { NearbyEvents } from "../pages/nearby-events/nearby-events";
import { Notifications } from "../pages/notifications/notifications";
import { HistoryPage } from "../pages/history-page/history-page";
import { FindEventsPage } from "../pages/find-events/find-events";
import { ProfilePage } from "../pages/profile-page/profile-page";
import { AboutUs } from "../pages/about-us/about-us";
import { EventComments } from "../pages/event-comments/event-comments";
import { ExpandNotification } from "../pages/expand-notification/expand-notification";
import { AddImagesVideos } from "../pages/add-images-videos/add-images-videos";
import { Feedback } from "../pages/feedback/feedback";


@NgModule({
  declarations: [
    MyApp,
    Start,
    TermsPage,
    DisclaimerPage,
    RegistrationPage,
    EventWall,
    CountryList,
    NearbyEvents,
    Notifications,
    HistoryPage,
    FindEventsPage,
    ProfilePage,
    AboutUs,
    EventComments,
    ExpandNotification,
    AddImagesVideos,
    Feedback
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    IonicImageViewerModule,
    ReactiveFormsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Start,
    TermsPage,
    DisclaimerPage,
    RegistrationPage,
    EventWall,
    CountryList,
    NearbyEvents,
    Notifications,
    HistoryPage,
    FindEventsPage,
    ProfilePage,
    AboutUs,
    EventComments,
    ExpandNotification,
    AddImagesVideos,
    Feedback
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Camera,
    Calendar,
    MediaCapture,
    File,
    Transfer,
    FilePath, 
    SQLite
  ]
})
export class AppModule {}

 export const AppParams = Object.freeze({
   BASE_PATH : "http://192.0.0.59:8080/Majlis-UserManagement-1.0/service/",
   ABOUT : 'Test about us http://192.0.0.48:8080/APIGateway-1.0/'
 })