import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, ToastController, Toast} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/network'

import { Start } from "../pages/start/start";
import { Notifications } from "../pages/notifications/notifications";
import { HistoryPage } from "../pages/history-page/history-page";
import { FindEventsPage } from "../pages/find-events/find-events";
import { ProfilePage } from "../pages/profile-page/profile-page";
import { AboutUs } from "../pages/about-us/about-us";
import { EventWall } from "../pages/event-wall/event-wall";

@Component({
  templateUrl: 'app.html',
  providers: [Network]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage:any = Start;
  static toast: Toast;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private toastCtrl: ToastController, private network: Network) {
    platform.ready().then(() => {


      this.network.onDisconnect().subscribe(() => {
        console.log("disconnect")
        MyApp.toast = toastCtrl.create({
          message: 'No internet connection '
        });
        MyApp.toast.present();
      });

      this.network.onConnect().subscribe(() => {
        console.log("connect")
        MyApp.toast.dismissAll();
      });

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  openNearbyEventsPage(){
    this.nav.setRoot(EventWall);
  }

  openNotificationsPage(){
        this.nav.push(Notifications);
  }

  openHistoryPage(){
        this.nav.push(HistoryPage);
  }

  openFindEventsPage(){
        this.nav.push(FindEventsPage);
  }

  openProfilePage(){
        this.nav.push(ProfilePage);
  }

  openAboutUsPage(){
        this.nav.push(AboutUs);
  }
}